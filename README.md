# README #

Common templates I use in eclipse.

name | description |
---- | ----------- |
_formap     | iterate over map  |
_logdef     |                   |
_logtrace   |                   |
_logdebug   |                   |
_loginfo    |                   |
_logwarning |                   |
_logerror   |${LOG}.error("", Exception)    |



```
#!java
//_formap
${:import(java.util.Map.Entry)}
for (Entry<${keyType:argType(map, 0)}, ${valueType:argType(map, 1)}> ${entry} : ${map:var(java.util.Map)}.entrySet())
{
    ${keyType} ${key} = ${entry}.getKey();
    ${valueType} ${value} = ${entry}.getValue();
    ${cursor}
}
```

```
#!java
//_logdef
${:import(org.apache.log4j.Logger)}
private static final Logger LOG = Logger.getLogger(${enclosing_type}.class);
```

```
#!java
//_logerror
${LOG:var(org.apache.log4j.Logger)}.error("${cursor}", ${e:var(java.lang.Throwable)});
```

### What is this repository for? ###

* Common templates I use in eclipse
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact